Source: golang-github-willf-bitset
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Shengjing Zhu <zhsj@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
Standards-Version: 4.5.0
Homepage: https://github.com/willf/bitset
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-willf-bitset
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-willf-bitset.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: github.com/willf/bitset

Package: golang-github-willf-bitset-dev
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Implements bitsets, a mapping between non-negative integers and boolean values
 This package provides a Go library with methods for setting, clearing,
 flipping, and testing individual integers.
 .
 It also provides set intersection, union, difference, complement, and
 symmetric operations, as well as tests to check whether any, all, or
 no bits are set, and queries a bitset's current length and number of
 positive bits.
